# Replace background behind the person for MS Teams / Zoom / Skype in Linux

## The goal of this project
The goal is to create a background replacer for MS Teams and similar video conferencing tools, since this feature is not usually not available on Linux. This code manipulates the video stream from webcam before it is streamed into video conferencing software, such that participant in the chat can not see the backgound behind the person.

## Disclaimer
To properly run this code you will need a graphics card capable of running tensorflow (typically NVidia graphics cards). It seems that tensorflow can be also executed on AMD, however I did not test this option (see: https://community.amd.com/t5/hsa/tensorflow-with-amd-gpu/td-p/199925). Running on a CPU is too slow for the practical usage.

## Dependencies
### This code was developed and tested in Ubuntu 18.04. First of all, install python and v4l2loopback-utils with commands:

sudo add-apt-repository ppa:deadsnakes/ppa<br />
sudo apt update<br />
sudo apt install python3.6<br />

sudo apt install v4l2loopback-utils <br />
sudo apt install ffmpeg <br />

## Go into the project directory and Install python-related modules:
pip3 install -r requirements.txt

## Setting up video devices
#### As the goal of the code is to:
 1) capture the original webcam stream, 
 2) manipulate it (remove background) and
 3) project it into MSTeams, 
  video devices should be configured first. 

### Figure out which is your input-stream device
Check and understand your existing video devices by first listing (the output will show you which video devices exist on your system):

#### ls /dev/video* 

You can check the video stream of each existing device with a command:

#### ffplay /dev/video0

Find the video device in which you use as a web cam and remember the number of this device, as you will need to set it as an --input-stream parameter later on. In the output of ffplay command you will be also able to see width and height of your video (for example 640x480). Remember those numbers as you will need them as an input parameter to a script.


### Create a new video device for output-stream
I typically have to first create additional video device, using a command:

#### sudo modprobe v4l2loopback devices=1

This command wll create an additional video device, used as an output-stream for manipulated video. Again execute this command to check the number of newly created video device:

#### ls /dev/video*

Than remember the number of newly created video device as you will need to set it as an --output-stream parameter when running the run.py script.

##### (Good to know) In case you e.g. made a mistake in modprobe command and you want to rerun modeprobe command, you have to first disable this kernel module using "rmmod v4l2loopback" and than run "modprobe v4l2loopback" with your parameters. 

## Run the script and set the parameters: 
Parameters: 

* --width (default=640): The width of you webcam footage
* --height (default=480): The height of you webcam footage
* --image (default='office.jpeg'): The path to background image
* --input-stream (default=0): The number of the video device, where webcam is recorded. E.g. if /dev/video0 is input stream, put 0 as a parameter.
* --output-stream (default=2): The number of the video device, where the modified video is streamed. E.g. if /dev/video2 is output stream, put 2 as a parameter.
op

### Example:
python3.6 run.py --input-stream 1 --output-stream 2

## Open up Teams and select the video device
Go to Settings  > Devices > Camera > 
and select your video device, with modified background.

## If you restart the computer, just rerun commands:
#### sudo modprobe v4l2loopback devices=1 
#### python3.6 run.py --input-stream 1 --output-stream 2

## References
https://github.com/jremmons/pyfakewebcam  
https://stackoverflow.com/questions/21446292/using-opencv-output-as-webcam  
https://github.com/umlaeute/v4l2loopback/  
